///////////////////////////////
DROP PROCEDURE IF EXISTS  `AltaCliente`//
 CREATE DEFINER=`root`@`localhost` PROCEDURE   AltaCliente (vNombre varchar(100),vDireccion varchar(100), vCP int (10), vTelefono varchar(15), vResponsable varchar(30))
BEGIN
INSERT INTO Cliente (Nombre,Direccion,CP,Telefono, Responsable) VALUES (vNombre,vDireccion,vCP,vTelefono, vResponsable);
END

////////////////////////////

DROP PROCEDURE IF EXISTS  `AltaEmpleado`//
 CREATE DEFINER=`root`@`localhost` PROCEDURE   AltaEmpleado (vNombre varchar(100),vPuesto varchar(100), vDireccion VARCHAR (100), vTelefono varchar(15), vFecha_Nacimiento Date)
BEGIN
INSERT INTO Empleado (Nombre,Puesto,Direccion,Telefono, Fecha_Nacimiento) VALUES (vNombre,vPuesto,vDireccion,vTelefono, vFecha_Nacimiento);
END

call AltaEmpleado('Julio Cesas Serraro Vazquez', 'Gerente General', 'Calle Juarez #898, Col. Centro, Yuriria Gto.','4454789087','09/09/1992')

///////////////////////////////


DROP PROCEDURE IF EXISTS  `AltaProductor`//
 CREATE DEFINER=`root`@`localhost` PROCEDURE   AltaProductor (vProducto_id int(3),vNombre varchar(100),	vDireccion varchar(100),	vCP int(10),vTelefono varchar(15) ,vResponsable varchar(30), vEmail varchar(30),vCodigo int(7))
BEGIN
INSERT INTO Productor (Producto_id ,Nombre ,Direccion ,CP ,Telefono ,Responsable , Email ,Codigo ) VALUES (vProducto_id ,vNombre ,vDireccion ,vCP ,vTelefono ,vResponsable , vEmail ,vCodigo);
END

  	call AltaProductor(1,'Vallado Leon', NULL, 38961,NULL,NULL,'villaleon@villa.mx',20012)
  	call AltaProductor(1,'Vallado Leon', 'leon #4 Guanajuato', 38961,'4451234455','leon gutierrez','villaleon@villa.mx',

////////////////////////////

DROP PROCEDURE IF EXISTS  `AltaProducto`//
 CREATE DEFINER=`root`@`localhost` PROCEDURE   AltaProducto (vNombre varchar(100))
BEGIN
INSERT INTO Producto (Nombre) VALUES (vNombre);
END

call AltaProducto('Jitomate')


//////////////////////////////////

DROP PROCEDURE IF EXISTS  `AltaMateriaPrima`//
 CREATE DEFINER=`root`@`localhost` PROCEDURE   AltaMateriaPrima (vProductor_id int(3),vFecha_Entrada date,
	vFecha_Proceso date,vPeso_Neto DECIMAL(7,2) ,vEstado CHAR(1))
BEGIN
INSERT INTO Materia_Prima (Productor_id,Fecha_Entrada,Fecha_Proceso,Peso_Neto,Estado) VALUES (vProductor_id,vFecha_Entrada,vFecha_Proceso,vPeso_Neto,vEstado);
END


call AltaMateriaPrima(1,'05-06-15','05-08-15',1678.90,'N')
////////////////////////////////////////////
UPDATE table_name
SET column1=value, column2=value2,...
WHERE some_column=some_value 



DROP PROCEDURE IF EXISTS  `EditarMateriaPrima`//
 CREATE DEFINER=`root`@`localhost` PROCEDURE   EditarMateriaPrima ( vMateriaP_id int(6) ,vProductor_id int(3),vFecha_Entrada date,
	vFecha_Proceso date,vPeso_Neto DECIMAL(7,2) ,vEstado CHAR(1))
BEGIN

UPDATE Materia_Prima SET Productor_id=vProductor_id,Fecha_Entrada=vFecha_Entrada, Fecha_Proceso=vFecha_Proceso,Peso_Neto=vPeso_Neto,Estado=vEstado
WHERE MateriaP_id=vMateriaP_id;

END







DROP PROCEDURE IF EXISTS  `EditarMateriaPrima`//
 CREATE DEFINER=`root`@`localhost` PROCEDURE   AltaMateriaPrima (vProductor_id int(3),vFecha_Entrada date,
	vFecha_Proceso date,vPeso_Neto DECIMAL(7,2) ,vEstado CHAR(1))
BEGIN
INSERT INTO Materia_Prima (Productor_id,Fecha_Entrada,Fecha_Proceso,Peso_Neto,Estado) VALUES (vProductor_id,vFecha_Entrada,vFecha_Proceso,vPeso_Neto,vEstado);
END




//////////////////////////////////

DROP PROCEDURE IF EXISTS  `AltaFoliosProducto`//
 CREATE DEFINER=`root`@`localhost` PROCEDURE   AltaFoliosProducto (vMateriaP_id int(6),vTamPes_id int(2),
	vDescripcion varchar(20),vCantidad int(3))
BEGIN
INSERT INTO Folios_Producto (MateriaP_id,TamPes_id,Descripcion,Cantidad) VALUES (vMateriaP_id,vTamPes_id,vDescripcion,vCantidad);
END

call AltaFoliosProducto(5,34,'Rojo',19)

///////////////////////////////////////7

